DB = ownership
VPATH = .:raw

define table_exists
    psql -d $(DB) -c "\d $@" 2>&1 > /dev/null
endef

all : exempt.geojson exempt.csv

parcels.geojson :
	esri2geojson --proxy "https://maps.cookcountyil.gov/cookviewer/proxy/tempProxy.ashx?" "https://gis1.cookcountyil.gov/arcgis/rest/services/cookVwrDynmc/MapServer/44" -v $@


parcels : parcels.geojson
	$(table_exists) || \
	     ogr2ogr -f PostgreSQL PG:dbname=$(DB) -t_srs EPSG:3435 -nln $@ $<

CommAreas.zip :
	wget -O $@ "https://data.cityofchicago.org/api/geospatial/cauq-8yn6?method=export&format=Original"

CommAreas.shp : CommAreas.zip
	unzip $<

community_area : CommAreas.shp
	ogr2ogr -f PostgreSQL PG:dbname=$(DB) -t_srs EPSG:3435 -nlt MULTIPOLYGON -lco precision=NO -nln $@ $<

kt.exempt_1.csv : kt.exempt.xls
	in2csv $< > $@

kt.exempt_2.csv : kt.exempt.xls
	in2csv $< --sheet='kt.exempt 2' > $@

assessor_exempt.csv : kt.exempt_1.csv kt.exempt_2.csv
	csvstack $^ | csvsort -c 2 > $@

% : %.csv
	$(table_exists) || \
            csvsql --db postgresql:///$(DB) --table $@ --insert "$<"

exempt.geojson : assessor_exempt parcels
	ogr2ogr -f "GeoJSON" $@ PG:"dbname=$(DB)" -t_srs EPSG:4326 \
            -lco COORDINATE_PRECISION=6 -sql \
            "SELECT parcels.*, \"EX_NAME\", \"EX_AGENCY\" FROM parcels INNER JOIN assessor_exempt \
             ON pin10 = left(lpad(\"EX_PIN\"::text, 14, '0'), 10)"

exempt.csv : exempt.geojson
	ogr2ogr -f csv $@ $<
