# Tax exempt properties in Cook County

Files:

- `exempt.geojson` parcels listed as tax exempted by the Cook County
  Tax Assessor. Includes details about the parcel pulled from
  https://maps.cookcountyil.gov/cookviewer including the parcel's tax
  code which can reveal something about the reason for the
  exemption. This files is suitable for mapping.
- `excemt.csv` the same data as a spreadsheet.

Source Files:

- [`raw/OWNED Property List w Addresses 20180119.xlsx`](https://github.com/datamade/non-profit-property/blob/master/raw/OWNED%20Property%20List%20w%20Addresses%2020180119.xlsx); Properties owned by Cook County. January, 2018 FOIA of Bureau of Asset Management 
- [`raw/kt.exempt.xls`](https://github.com/datamade/non-profit-property/blob/master/raw/kt.exempt.xls); Tax exempt properties in Cook County. [December, 2018 FOIA of Cook County Tax Assessor](https://www.muckrock.com/foi/cook-county-365/exempt-parcels-and-exempt-for-cook-county-il-refiled-53230/)
- [`raw/facilities.csv`](raw/facilities.csv); Properties managed by University of Chicago's Facilities Services. [Juliet Eldred's Thesis](https://www.arcgis.com/apps/MapSeries/index.html?appid=0b4a3b97c82540e7bb3350550c92282b&wmode=opaque)

# To build `exempt.geojson` and `exempt.csv`
 
You need to have postgres and postgis installed on the system already

```bash
pip install -r requirements
createdb ownership
psql -d ownershp -c "create extension postgis"
make all
```
